<!-- markdownlint-disable MD041 -->
## Summary

(Summarize the bug encountered concisely)

## Versions used to reproduce issue

(Indicate all relevant information about your system, including as many details as possible.)
(**Most importantly, specify which version of the software you are using.**)

- are you using ScenicOS?
  - [ ] Yes
    - ScenicOS version: _scenicos-focal-amd64-x86-64-1.0.0-build133-be323984-2022-08-22-1734_ (cat /image_version)
  - [ ] No
    - linux distribution: Ubuntu 20.04.5 LTS (lsb_release -sd)
    - system info: Linux ubuntu 5.4.0-126-generic #142-Ubuntu SMP Fri Aug 26 12:12:57 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux (uname -a)
- [ ] required versions
  - shmdata: _1.3.70_
  - ndi2shmdata: _0.6.0_
  - switcher: _develop@942b299b_ (Sep 30, 2022 2:16pm GMT-0400)
  - scenic-core: _4.0.6_
  - scenic: _4.0.8_ (scenic-app.ca)
- [ ] if it is related to QA tests:
  - scenic-selenium: _develop@a6c0fcde_ (Sep 26, 2022 12:11pm GMT-0400)
- [ ] if this bug occurred during a SIP call:
  - sip-server: _dev.sip.scenic.sat.qc.ca/2.1.0_
- [ ] other versions
  - python: 3.8.10 (python3 --version)
  - python-socketio: 5.7.1 (pip list | grep python-socketio)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## What is the frequency of occurrence of this behavior ?

(Describe if the issue appears every time, sometimes, etc. You can also mention here if this issue is a regression.)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

## Additional comments

(Add references to related issues and provide any extra comments.)

/label ~"on hold" ~"priority::P3"
/label ~"type::bug" ~"severity::major"

/cc @flecavalier
/assign @vlaurent
